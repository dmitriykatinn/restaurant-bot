package ru.handh.tgbot.callback;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import ru.handh.tgbot.model.Order;
import ru.handh.tgbot.model.OrderDetailed;
import ru.handh.tgbot.model.Product;
import ru.handh.tgbot.service.OrderService;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Component
@RequiredArgsConstructor
public class OrdersCallback implements Callback {

    private final OrderService orderService;

    @Override
    public List<BotApiMethod<? extends Serializable>> apply(Update update) {
        long chatId = update.getCallbackQuery().getMessage().getChatId();
        int messageId = update.getCallbackQuery().getMessage().getMessageId();
        long userId = update.getCallbackQuery().getFrom().getId();
        EditMessageText messageText = new EditMessageText();
        messageText.setChatId(String.valueOf(chatId));
        messageText.setText("Ваши заказы:");
        messageText.setMessageId(messageId);

        addKeyboard(messageText, userId);
        return List.of(messageText);
    }

    private void addKeyboard(EditMessageText messageText, long userId) {
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rowList = new ArrayList<>();

        InlineKeyboardButton backButton = new InlineKeyboardButton();
        backButton.setText("Назад");
        backButton.setCallbackData("start");
        rowList.add(List.of(backButton));

        List<OrderDetailed> orders = orderService.getOrdersByOwner(userId);
        for(OrderDetailed order : orders) {
            InlineKeyboardButton categoryButton = new InlineKeyboardButton();
            categoryButton.setText("Заказ №" + order.getId());
            categoryButton.setCallbackData("choose_order " + order.getId());
            rowList.add(List.of(categoryButton));
        }

        inlineKeyboardMarkup.setKeyboard(rowList);
        messageText.setReplyMarkup(inlineKeyboardMarkup);
    }
}
