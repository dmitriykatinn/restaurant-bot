package ru.handh.tgbot.callback;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import ru.handh.tgbot.model.Product;
import ru.handh.tgbot.service.ProductService;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Component
@RequiredArgsConstructor
public class MenuChooseCategoryCallback implements Callback {

    private final ProductService productService;

    @Override
    public List<BotApiMethod<? extends Serializable>> apply(Update update) {
        String callbackQueryData = update.getCallbackQuery().getData();
        int categoryId = Integer.parseInt(callbackQueryData.split(" ")[1]);

        long chatId = update.getCallbackQuery().getMessage().getChatId();
        int messageId = update.getCallbackQuery().getMessage().getMessageId();
        EditMessageText messageText = new EditMessageText();
        messageText.setChatId(String.valueOf(chatId));
        messageText.setMessageId(messageId);
        messageText.setText("Нажмите на кнопку для добавления блюда в заказ");

        addKeyboard(messageText, categoryId);
        return List.of(messageText);
    }

    private void addKeyboard(EditMessageText messageText, int categoryId) {
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rowList = new ArrayList<>();

        InlineKeyboardButton backButton = new InlineKeyboardButton();
        backButton.setText("Назад");
        backButton.setCallbackData("menu");
        rowList.add(List.of(backButton));

        List<Product> products = productService.getProductsByCategory(categoryId);
        for(Product product : products) {
            InlineKeyboardButton categoryButton = new InlineKeyboardButton();
            categoryButton.setText(product.getName() + " - " + product.getPrice());
            categoryButton.setCallbackData("choose_product " + product.getId());
            rowList.add(List.of(categoryButton));
        }

        InlineKeyboardButton makeOrderButton = new InlineKeyboardButton();
        makeOrderButton.setText("Сделать заказ");
        makeOrderButton.setCallbackData("make_order");
        rowList.add(List.of(makeOrderButton));

        inlineKeyboardMarkup.setKeyboard(rowList);
        messageText.setReplyMarkup(inlineKeyboardMarkup);
    }
}