package ru.handh.tgbot.callback;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.meta.api.objects.Update;
import ru.handh.tgbot.model.Order;
import ru.handh.tgbot.model.OrderLine;
import ru.handh.tgbot.model.UpdateUserRequest;
import ru.handh.tgbot.model.User;
import ru.handh.tgbot.service.OrderService;
import ru.handh.tgbot.service.UserService;
import ru.handh.tgbot.util.ShoppingCart;
import ru.handh.tgbot.util.ShoppingCartHolder;

import java.io.Serializable;
import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class PaymentCallback implements Callback {

    private final ShoppingCartHolder shoppingCartHolder;
    private final OrderService orderService;
    private final UserService userService;

    @Override
    public List<BotApiMethod<? extends Serializable>> apply(Update update) {
        long userId = update.getCallbackQuery().getFrom().getId();
        ShoppingCart shoppingCart = shoppingCartHolder.getShoppingCart(userId);
        Order order = Order.builder()
                .ownerId(userId)
                .status("PREPARING")
                .content(shoppingCart.getContent().entrySet().stream()
                        .map(entry -> new OrderLine(entry.getKey().getId(), entry.getValue()))
                        .collect(Collectors.toList()))
                .build();
        orderService.createOrder(order);

        User user = userService.findById(userId);
        int bonusPointsDeducted = (int) Math.min(user.getBonusPoints(), shoppingCart.cost());
        double amountToBePaid = shoppingCart.cost() - bonusPointsDeducted;
        int bonusPointsCredited = (int) (amountToBePaid * 0.1);
        UpdateUserRequest updateUserRequest = new UpdateUserRequest();
        updateUserRequest.setBonusPoints((int) (user.getBonusPoints() - bonusPointsDeducted + bonusPointsCredited));
        updateUserRequest.setLastAction(Instant.now());
        userService.updateUser(userId, updateUserRequest);

        shoppingCart.clear();

        long chatId = update.getCallbackQuery().getMessage().getChatId();
        int messageId = update.getCallbackQuery().getMessage().getMessageId();
        EditMessageText messageText = new EditMessageText();
        messageText.setChatId(String.valueOf(chatId));
        messageText.setMessageId(messageId);
        messageText.setText("Заказ оплачен. Вам начислено " + bonusPointsCredited + " бонусов.\n"
                + "Вы получите уведомление, когда заказ будет готов.");

        return List.of(messageText);
    }
}
