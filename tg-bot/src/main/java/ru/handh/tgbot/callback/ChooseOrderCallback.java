package ru.handh.tgbot.callback;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import ru.handh.tgbot.model.OrderDetailed;
import ru.handh.tgbot.model.Product;
import ru.handh.tgbot.service.OrderService;
import ru.handh.tgbot.service.ProductService;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
@RequiredArgsConstructor
public class ChooseOrderCallback implements Callback {

    private final OrderService orderService;
    private final ProductService productService;

    @Override
    public List<BotApiMethod<? extends Serializable>> apply(Update update) {
        String callbackQueryData = update.getCallbackQuery().getData();
        int orderId = Integer.parseInt(callbackQueryData.split(" ")[1]);

        OrderDetailed order = orderService.findById(orderId);

        long chatId = update.getCallbackQuery().getMessage().getChatId();
        int messageId = update.getCallbackQuery().getMessage().getMessageId();
        EditMessageText messageText = new EditMessageText();
        messageText.setChatId(String.valueOf(chatId));
        messageText.setMessageId(messageId);
        messageText.setText(getOrderMessage(order));

        addKeyboard(messageText);
        return List.of(messageText);
    }

    private String getOrderMessage(OrderDetailed order) {
        final StringBuilder sb = new StringBuilder()
                .append("Заказ №").append(order.getId()).append(":\n\n")
                .append("Статус: ").append(order.getStatus()).append("\n\n")
                .append("Содержимое:\n");
        order.getContent().forEach(orderLine -> {
                    Product product = productService.findById(orderLine.getProductId());
                    int amount = orderLine.getAmount();
                    sb.append(product.getName()).append(" x ").append(amount).append(" --- ")
                            .append(product.getPrice()*amount).append("\n");
                });

        return sb.toString();
    }

    private void addKeyboard(EditMessageText messageText) {
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rowList = new ArrayList<>();

        InlineKeyboardButton backButton = new InlineKeyboardButton();
        backButton.setText("Назад");
        backButton.setCallbackData("orders");
        rowList.add(List.of(backButton));

        inlineKeyboardMarkup.setKeyboard(rowList);
        messageText.setReplyMarkup(inlineKeyboardMarkup);
    }
}
