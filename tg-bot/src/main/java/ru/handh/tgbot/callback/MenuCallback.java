package ru.handh.tgbot.callback;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import ru.handh.tgbot.model.Category;
import ru.handh.tgbot.service.ProductService;

import java.io.Serializable;
import java.util.*;

@Component
@RequiredArgsConstructor
public class MenuCallback implements Callback {

    private final ProductService productService;

    @Override
    public List<BotApiMethod<? extends Serializable>> apply(Update update) {
        long chatId = update.getCallbackQuery().getMessage().getChatId();
        int messageId = update.getCallbackQuery().getMessage().getMessageId();
        EditMessageText messageText = new EditMessageText();
        messageText.setChatId(String.valueOf(chatId));
        messageText.setText("Выберите категорию меню:");
        messageText.setMessageId(messageId);

        addKeyboard(messageText);
        return List.of(messageText);
    }

    private void addKeyboard(EditMessageText messageText) {
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rowList = new ArrayList<>();

        InlineKeyboardButton backButton = new InlineKeyboardButton();
        backButton.setText("Назад");
        backButton.setCallbackData("start");
        rowList.add(List.of(backButton));

        List<Category> categories = productService.getCategories();
        for(Category category : categories) {
            InlineKeyboardButton categoryButton = new InlineKeyboardButton();
            categoryButton.setText(String.valueOf(category.getName()));
            categoryButton.setCallbackData("choose_category " + category.getId());
            rowList.add(List.of(categoryButton));
        }

        inlineKeyboardMarkup.setKeyboard(rowList);
        messageText.setReplyMarkup(inlineKeyboardMarkup);
    }
}
