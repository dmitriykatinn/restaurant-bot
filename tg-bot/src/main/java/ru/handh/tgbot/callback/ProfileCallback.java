package ru.handh.tgbot.callback;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import ru.handh.tgbot.model.User;
import ru.handh.tgbot.service.UserService;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
@RequiredArgsConstructor
public class ProfileCallback implements Callback {

    private final UserService userService;

    @Override
    public List<BotApiMethod<? extends Serializable>> apply(Update update) {
        long chatId = update.getCallbackQuery().getMessage().getChatId();
        int messageId = update.getCallbackQuery().getMessage().getMessageId();
        long userId = update.getCallbackQuery().getFrom().getId();

        User user = userService.findById(userId);
        String text = "Профиль:\n" +
                "ID: " + user.getId() + "\n" +
                "Имя: " + user.getName() + "\n" +
                "Бонусные баллы: " + user.getBonusPoints();

        EditMessageText messageText = new EditMessageText();
        messageText.setChatId(String.valueOf(chatId));
        messageText.setText(text);
        messageText.setMessageId(messageId);

        addKeyboard(messageText);
        return List.of(messageText);
    }

    private void addKeyboard(EditMessageText messageText) {
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rowList = new ArrayList<>();

        InlineKeyboardButton backButton = new InlineKeyboardButton();
        backButton.setText("Назад");
        backButton.setCallbackData("start");
        rowList.add(List.of(backButton));

        inlineKeyboardMarkup.setKeyboard(rowList);
        messageText.setReplyMarkup(inlineKeyboardMarkup);
    }
}
