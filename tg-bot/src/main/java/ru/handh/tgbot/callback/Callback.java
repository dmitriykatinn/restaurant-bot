package ru.handh.tgbot.callback;

import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.objects.Update;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

public interface Callback {
    List<BotApiMethod<? extends Serializable>> apply(Update update);
}
