package ru.handh.tgbot.callback;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import ru.handh.tgbot.model.User;
import ru.handh.tgbot.service.UserService;
import ru.handh.tgbot.util.ShoppingCart;
import ru.handh.tgbot.util.ShoppingCartHolder;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
@RequiredArgsConstructor
public class MakeOrderCallback implements Callback {

    private final ShoppingCartHolder shoppingCartHolder;
    private final UserService userService;

    @Override
    public List<BotApiMethod<? extends Serializable>> apply(Update update) {
        long userId = update.getCallbackQuery().getFrom().getId();
        ShoppingCart shoppingCart = shoppingCartHolder.getShoppingCart(userId);
        User user = userService.findById(userId);
        int bonusPointsDeducted = (int) Math.min(user.getBonusPoints(), shoppingCart.cost());

        String text = "Ваш заказ:\n" + shoppingCart
                + "Стоимость: " + shoppingCart.cost() + "\n"
                + "Будет списано " + bonusPointsDeducted + " бонусных баллов.\n"
                + "К оплате: " + (shoppingCart.cost() - bonusPointsDeducted);

        long chatId = update.getCallbackQuery().getMessage().getChatId();
        int messageId = update.getCallbackQuery().getMessage().getMessageId();
        EditMessageText messageText = new EditMessageText();
        messageText.setChatId(String.valueOf(chatId));
        messageText.setMessageId(messageId);
        messageText.setText(text);

        addKeyboard(messageText);
        return List.of(messageText);
    }

    private void addKeyboard(EditMessageText messageText) {
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rowList = new ArrayList<>();

        InlineKeyboardButton paymentButton = new InlineKeyboardButton();
        paymentButton.setText("Оплатить");
        paymentButton.setCallbackData("payment");
        rowList.add(List.of(paymentButton));

        InlineKeyboardButton cancelButton = new InlineKeyboardButton();
        cancelButton.setText("Отменить");
        cancelButton.setCallbackData("start");
        rowList.add(List.of(cancelButton));

        inlineKeyboardMarkup.setKeyboard(rowList);
        messageText.setReplyMarkup(inlineKeyboardMarkup);
    }
}