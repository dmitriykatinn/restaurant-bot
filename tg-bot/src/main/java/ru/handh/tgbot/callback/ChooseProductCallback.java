package ru.handh.tgbot.callback;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.objects.Update;
import ru.handh.tgbot.model.Product;
import ru.handh.tgbot.service.ProductService;
import ru.handh.tgbot.util.ShoppingCart;
import ru.handh.tgbot.util.ShoppingCartHolder;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Component
@RequiredArgsConstructor
public class ChooseProductCallback implements Callback {

    private final ShoppingCartHolder shoppingCartHolder;
    private final ProductService productService;

    @Override
    public List<BotApiMethod<? extends Serializable>> apply(Update update) {
        long userId = update.getCallbackQuery().getFrom().getId();
        String callbackQueryData = update.getCallbackQuery().getData();
        int productId = Integer.parseInt(callbackQueryData.split(" ")[1]);

        ShoppingCart shoppingCart = shoppingCartHolder.getShoppingCart(userId);
        Product product = productService.findById(productId);
        shoppingCart.add(product);
        return Collections.emptyList();
    }
}

