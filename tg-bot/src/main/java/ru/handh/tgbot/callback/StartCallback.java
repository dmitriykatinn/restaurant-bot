package ru.handh.tgbot.callback;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import ru.handh.tgbot.util.ShoppingCart;
import ru.handh.tgbot.util.ShoppingCartHolder;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
@RequiredArgsConstructor
public class StartCallback implements Callback {

    private final ShoppingCartHolder shoppingCartHolder;

    @Override
    public List<BotApiMethod<? extends Serializable>> apply(Update update) {
        long userId = update.getCallbackQuery().getFrom().getId();
        ShoppingCart shoppingCart = shoppingCartHolder.getShoppingCart(userId);
        shoppingCart.clear();

        long chatId = update.getCallbackQuery().getMessage().getChatId();
        int messageId = update.getCallbackQuery().getMessage().getMessageId();
        EditMessageText messageText = new EditMessageText();
        messageText.setChatId(String.valueOf(chatId));
        messageText.setText("Выберите интересующий вас раздел:");
        messageText.setMessageId(messageId);

        addKeyboard(messageText);
        return List.of(messageText);
    }

    private void addKeyboard(EditMessageText messageText) {
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rowList = new ArrayList<>();

        InlineKeyboardButton menuButton = new InlineKeyboardButton();
        menuButton.setText("Меню");
        menuButton.setCallbackData("menu");
        rowList.add(List.of(menuButton));

        InlineKeyboardButton profileButton = new InlineKeyboardButton();
        profileButton.setText("Профиль");
        profileButton.setCallbackData("profile");
        rowList.add(List.of(profileButton));

        InlineKeyboardButton ordersButton = new InlineKeyboardButton();
        ordersButton.setText("Заказы");
        ordersButton.setCallbackData("orders");
        rowList.add(List.of(ordersButton));

        inlineKeyboardMarkup.setKeyboard(rowList);
        messageText.setReplyMarkup(inlineKeyboardMarkup);
    }
}
