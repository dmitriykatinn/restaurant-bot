package ru.handh.tgbot.callback;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.DeleteMessage;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.meta.api.objects.Update;
import ru.handh.tgbot.model.OrderDetailed;
import ru.handh.tgbot.model.User;
import ru.handh.tgbot.service.OrderService;
import ru.handh.tgbot.service.UserService;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

@Component
@RequiredArgsConstructor
public class OrderReadyCallback implements Callback {

    private final OrderService orderService;

    @Override
    public List<BotApiMethod<? extends Serializable>> apply(Update update) {
        long chatId = update.getCallbackQuery().getMessage().getChatId();
        int messageId = update.getCallbackQuery().getMessage().getMessageId();
        DeleteMessage deleteMessage = new DeleteMessage();
        deleteMessage.setChatId(String.valueOf(chatId));
        deleteMessage.setMessageId(messageId);

        String callbackQueryData = update.getCallbackQuery().getData();
        int orderId = Integer.parseInt(callbackQueryData.split(" ")[1]);
        orderService.updateStatus(orderId, "READY");

        return List.of(deleteMessage);
    }
}
