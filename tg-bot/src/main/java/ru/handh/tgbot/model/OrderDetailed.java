package ru.handh.tgbot.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class OrderDetailed {
    private int id;
    private long ownerId;
    private String status;
    private List<OrderLine> content;
}
