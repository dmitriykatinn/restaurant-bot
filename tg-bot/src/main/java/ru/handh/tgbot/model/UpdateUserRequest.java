package ru.handh.tgbot.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UpdateUserRequest {
    private String role;
    private Integer bonusPoints;
    private Instant lastAction;
}
