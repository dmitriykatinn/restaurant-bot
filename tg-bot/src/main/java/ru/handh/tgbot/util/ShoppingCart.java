package ru.handh.tgbot.util;

import lombok.Data;
import ru.handh.tgbot.model.Product;

import java.util.HashMap;
import java.util.Map;

public class ShoppingCart {

    private Map<Product, Integer> content = new HashMap<>();

    public void add(Product product) {
        content.merge(product, 1, Integer::sum);
    }

    public void clear() {
        content.clear();
    }

    public Map<Product, Integer> getContent() {
        return content;
    }

    public double cost() {
        double sum = 0;
        for(Map.Entry<Product, Integer> entry : content.entrySet()) {
            Product product = entry.getKey();
            int amount = entry.getValue();
            sum += product.getPrice()*amount;
        }
        return sum;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        for(Map.Entry<Product, Integer> entry : content.entrySet()) {
            Product product = entry.getKey();
            int amount = entry.getValue();
            sb.append(product.getName()).append(" x ").append(amount).append(" --- ")
                    .append(product.getPrice()*amount).append("\n");
        }
        return sb.toString();
    }
}
