package ru.handh.tgbot.util;

import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class ShoppingCartHolder {

    private final Map<Long, ShoppingCart> shoppingCarts = new HashMap<>();

    public ShoppingCart getShoppingCart(long userId) {
        ShoppingCart shoppingCart = shoppingCarts.get(userId);
        if(shoppingCart == null) {
            shoppingCart = new ShoppingCart();
            shoppingCarts.put(userId, shoppingCart);
        }
        return shoppingCart;
    }
}
