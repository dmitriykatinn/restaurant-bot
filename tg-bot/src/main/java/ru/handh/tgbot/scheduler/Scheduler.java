package ru.handh.tgbot.scheduler;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import ru.handh.tgbot.listener.BotListenerLongPoll;
import ru.handh.tgbot.model.UpdateUserRequest;
import ru.handh.tgbot.model.User;
import ru.handh.tgbot.service.UserService;

import java.time.Instant;
import java.util.List;
import java.util.concurrent.TimeUnit;

@EnableScheduling
@Slf4j
@Component
@RequiredArgsConstructor
public class Scheduler {

    private final UserService userService;
    private final BotListenerLongPoll botListenerLongPoll;

    @Scheduled(cron = "0 0 18 * * *")
    public void notifyUsersWithLastActionWeekMoreAgo() {
        Instant weekAgo = Instant.now().minusSeconds(7*24*60*60);
        userService.findAll().stream()
                .filter(user -> user.getLastAction().compareTo(weekAgo) < 0)
                .forEach(user -> {
                    UpdateUserRequest updateUserRequest = new UpdateUserRequest();
                    updateUserRequest.setLastAction(Instant.now());
                    userService.updateUser(user.getId(), updateUserRequest);
                    SendMessage sendMessage = new SendMessage(String.valueOf(user.getChatId()),
                            "Вы давно к нам не заходили. Советуем сделать заказ!");
                    try {
                        botListenerLongPoll.execute(sendMessage);
                    } catch (TelegramApiException e) {
                        log.error(e.getMessage());
                    }
                });
    }
}
