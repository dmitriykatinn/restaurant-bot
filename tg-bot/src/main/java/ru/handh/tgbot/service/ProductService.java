package ru.handh.tgbot.service;

import ru.handh.tgbot.model.Category;
import ru.handh.tgbot.model.Product;

import java.util.List;

public interface ProductService {
    List<Category> getCategories();

    List<Product> getProductsByCategory(int categoryId);

    Product findById(int productId);
}
