package ru.handh.tgbot.service.impl;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import ru.handh.tgbot.model.*;
import ru.handh.tgbot.service.OrderService;

import java.util.List;

@Service
public class OrderServiceImpl implements OrderService {

    @Value("${order-service.path}")
    private String servicePath;

    @Override
    public OrderDetailed createOrder(Order order) {
        return WebClient.builder()
                    .baseUrl(servicePath)
                    .build()
                .post()
                .accept(MediaType.APPLICATION_JSON)
                .bodyValue(order)
                .retrieve()
                .bodyToMono(OrderDetailed.class)
                .block();
    }

    @Override
    public List<OrderDetailed> getOrdersByOwner(long userId) {
        return WebClient.builder()
                    .baseUrl(servicePath)
                    .build()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .queryParam("ownerId", String.valueOf(userId))
                        .build())
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToMono(new ParameterizedTypeReference<List<OrderDetailed>>() {})
                .block();
    }

    @Override
    public OrderDetailed findById(int orderId) {
        return WebClient.builder()
                    .baseUrl(servicePath)
                    .build()
                .get()
                .uri("/{orderId}", orderId)
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToMono(OrderDetailed.class)
                .block();
    }

    @Override
    public OrderDetailed updateStatus(int orderId, String status) {
        return WebClient.builder()
                    .baseUrl(servicePath)
                    .build()
                .patch()
                .uri("/{orderId}", orderId)
                .accept(MediaType.APPLICATION_JSON)
                .bodyValue(new UpdateOrderStatusRequest(status))
                .retrieve()
                .bodyToMono(OrderDetailed.class)
                .block();
    }
}
