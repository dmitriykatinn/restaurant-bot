package ru.handh.tgbot.service;

import ru.handh.tgbot.model.Order;
import ru.handh.tgbot.model.OrderDetailed;

import java.util.List;

public interface OrderService {
    OrderDetailed createOrder(Order order);

    List<OrderDetailed> getOrdersByOwner(long userId);

    OrderDetailed findById(int orderId);

    OrderDetailed updateStatus(int orderId, String status);
}
