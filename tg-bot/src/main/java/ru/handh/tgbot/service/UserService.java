package ru.handh.tgbot.service;

import ru.handh.tgbot.model.UpdateUserRequest;
import ru.handh.tgbot.model.User;

import java.util.List;

public interface UserService {
    User findById(long userId);

    User createUser(User user);

    List<User> findByRole(String role);

    List<User> findAll();

    User updateUser(long userId, UpdateUserRequest updateUserRequest);
}
