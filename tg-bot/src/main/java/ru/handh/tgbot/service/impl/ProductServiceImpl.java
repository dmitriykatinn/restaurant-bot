package ru.handh.tgbot.service.impl;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import ru.handh.tgbot.model.Category;
import ru.handh.tgbot.model.Product;
import ru.handh.tgbot.model.User;
import ru.handh.tgbot.service.ProductService;

import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {

    @Value("${product-service.path}")
    private String servicePath;

    @Override
    public List<Category> getCategories() {
        return WebClient.builder()
                    .baseUrl(servicePath)
                    .build()
                .get()
                .uri("/categories")
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToMono(new ParameterizedTypeReference<List<Category>>() {})
                .block();
    }

    @Override
    public List<Product> getProductsByCategory(int categoryId) {
        return WebClient.builder()
                    .baseUrl(servicePath)
                    .build()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .queryParam("categoryId", String.valueOf(categoryId))
                        .build())
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToMono(new ParameterizedTypeReference<List<Product>>() {})
                .block();
    }

    @Override
    public Product findById(int productId) {
        return WebClient.builder()
                    .baseUrl(servicePath)
                    .build()
                .get()
                .uri("/{productId}", productId)
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToMono(Product.class)
                .block();
    }
}
