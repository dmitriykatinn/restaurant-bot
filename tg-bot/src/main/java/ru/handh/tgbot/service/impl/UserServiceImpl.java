package ru.handh.tgbot.service.impl;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import ru.handh.tgbot.model.Category;
import ru.handh.tgbot.model.Product;
import ru.handh.tgbot.model.UpdateUserRequest;
import ru.handh.tgbot.model.User;
import ru.handh.tgbot.service.UserService;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Value("${user-service.path}")
    private String servicePath;

    @Override
    public User findById(long userId) {
        return WebClient.builder()
                    .baseUrl(servicePath)
                    .build()
                .get()
                .uri("/{userId}", userId)
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToMono(User.class)
                .block();
    }

    @Override
    public User createUser(User user) {
        return WebClient.builder()
                    .baseUrl(servicePath)
                    .build()
                .post()
                .accept(MediaType.APPLICATION_JSON)
                .bodyValue(user)
                .retrieve()
                .bodyToMono(User.class)
                .block();
    }

    @Override
    public List<User> findByRole(String role) {
        return WebClient.builder()
                    .baseUrl(servicePath)
                    .build()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .queryParam("role", role)
                        .build())
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToMono(new ParameterizedTypeReference<List<User>>() {})
                .block();
    }

    @Override
    public List<User> findAll() {
        return WebClient.builder()
                    .baseUrl(servicePath)
                    .build()
                .get()
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToMono(new ParameterizedTypeReference<List<User>>() {})
                .block();
    }

    @Override
    public User updateUser(long userId, UpdateUserRequest updateUserRequest) {
        return WebClient.builder()
                    .baseUrl(servicePath)
                    .build()
                .patch()
                .uri("/{userId}", userId)
                .accept(MediaType.APPLICATION_JSON)
                .bodyValue(updateUserRequest)
                .retrieve()
                .bodyToMono(User.class)
                .block();
    }
}
