package ru.handh.tgbot.command;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import ru.handh.tgbot.model.UpdateUserRequest;
import ru.handh.tgbot.model.User;
import ru.handh.tgbot.service.UserService;

import java.io.Serializable;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
@RequiredArgsConstructor
public class StartCommand implements Command {

    private final UserService userService;

    @Override
    public List<BotApiMethod<? extends Serializable>> apply(Update update) {
        long chatId = update.getMessage().getChatId();
        long userId = update.getMessage().getFrom().getId();
        String firstName = update.getMessage().getFrom().getFirstName();
        try {
            User user = userService.findById(userId);
            UpdateUserRequest updateUserRequest = new UpdateUserRequest();
            updateUserRequest.setLastAction(Instant.now());
            userService.updateUser(userId, updateUserRequest);
        } catch (WebClientResponseException e) {
            User user = User.builder()
                    .id(userId)
                    .name(firstName)
                    .role("CLIENT")
                    .bonusPoints(100)
                    .chatId(chatId)
                    .build();
            userService.createUser(user);
        }

        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(String.valueOf(chatId));
        sendMessage.setText("Выберите интересующий вас раздел:");

        addKeyboard(sendMessage);
        return List.of(sendMessage);
    }

    private void addKeyboard(SendMessage sendMessage) {
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rowList = new ArrayList<>();

        InlineKeyboardButton menuButton = new InlineKeyboardButton();
        menuButton.setText("Меню");
        menuButton.setCallbackData("menu");
        rowList.add(List.of(menuButton));

        InlineKeyboardButton profileButton = new InlineKeyboardButton();
        profileButton.setText("Профиль");
        profileButton.setCallbackData("profile");
        rowList.add(List.of(profileButton));

        InlineKeyboardButton ordersButton = new InlineKeyboardButton();
        ordersButton.setText("Заказы");
        ordersButton.setCallbackData("orders");
        rowList.add(List.of(ordersButton));

        inlineKeyboardMarkup.setKeyboard(rowList);
        sendMessage.setReplyMarkup(inlineKeyboardMarkup);
    }
}
