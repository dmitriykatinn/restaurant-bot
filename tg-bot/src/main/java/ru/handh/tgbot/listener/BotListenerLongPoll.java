package ru.handh.tgbot.listener;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import ru.handh.tgbot.handler.CallbacksHandler;
import ru.handh.tgbot.handler.CommandsHandler;
import ru.handh.tgbot.property.TelegramBotProperty;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

@Component
@Slf4j
public class BotListenerLongPoll extends TelegramLongPollingBot {

    private final TelegramBotProperty telegramBotProperty;
    private final CommandsHandler commandsHandler;
    private final CallbacksHandler callbacksHandler;

    public BotListenerLongPoll(TelegramBotProperty telegramBotProperty,
                               CommandsHandler commandsHandler,
                               CallbacksHandler callbacksHandler) {
        super(telegramBotProperty.getToken());
        this.telegramBotProperty = telegramBotProperty;
        this.commandsHandler = commandsHandler;
        this.callbacksHandler = callbacksHandler;
    }

    @Override
    public String getBotUsername() {
        return telegramBotProperty.getName();
    }

    @Override
    public void onUpdateReceived(Update update) {
        if (update.hasMessage() && update.getMessage().hasText()) {
            if (update.getMessage().getText().startsWith("/")) {
                executeAll(commandsHandler.handleCommands(update));
            }
        } else if (update.hasCallbackQuery()) {
            executeAll(callbacksHandler.handleCallbacks(update));
        }
    }

    private void executeAll(List<BotApiMethod<? extends Serializable>> methods) {
        try {
            for(BotApiMethod<? extends Serializable> method : methods) {
                execute(method);
            }
        } catch (TelegramApiException e) {
            log.error(e.getMessage());
        }
    }
}