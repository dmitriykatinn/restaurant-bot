package ru.handh.tgbot.listener;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import ru.handh.tgbot.model.OrderDetailed;
import ru.handh.tgbot.model.Product;
import ru.handh.tgbot.model.User;
import ru.handh.tgbot.service.OrderService;
import ru.handh.tgbot.service.ProductService;
import ru.handh.tgbot.service.UserService;

import java.util.ArrayList;
import java.util.List;

@Component
@Slf4j
@RequiredArgsConstructor
public class OrderServiceListener {

    private final BotListenerLongPoll botListenerLongPoll;
    private final OrderService orderService;
    private final ProductService productService;
    private final UserService userService;

    @KafkaListener(topics = "ORDER_CREATED", groupId = "test")
    public void sendOrderToEmployee(int orderId) {
        OrderDetailed order = orderService.findById(orderId);
        List<User> employees = userService.findByRole("EMPLOYEE");
        String orderMessage = getOrderMessage(order);
        for(User employee : employees) {
            SendMessage sendMessage = new SendMessage();
            sendMessage.setChatId(employee.getChatId());
            sendMessage.setText(orderMessage);

            InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
            List<List<InlineKeyboardButton>> rowList = new ArrayList<>();
            InlineKeyboardButton readyButton = new InlineKeyboardButton();
            readyButton.setText("Готов");
            readyButton.setCallbackData("order_ready " + orderId);
            rowList.add(List.of(readyButton));
            inlineKeyboardMarkup.setKeyboard(rowList);
            sendMessage.setReplyMarkup(inlineKeyboardMarkup);

            try {
                botListenerLongPoll.execute(sendMessage);
            } catch (TelegramApiException e) {
                log.error(e.getMessage());
            }
        }
    }

    @KafkaListener(topics = "ORDER_STATUS_UPDATED", groupId = "test")
    public void sendOrderStatusToOwner(int orderId) {
        OrderDetailed order = orderService.findById(orderId);
        User owner = userService.findById(order.getOwnerId());
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(owner.getChatId());
        sendMessage.setText("Заказ №" + orderId + " готов!");
        try {
            botListenerLongPoll.execute(sendMessage);
        } catch (TelegramApiException e) {
            log.error(e.getMessage());
        }
    }

    private String getOrderMessage(OrderDetailed order) {
        final StringBuilder sb = new StringBuilder()
                .append("Заказ №").append(order.getId()).append(":\n\n")
                .append("Статус: ").append(order.getStatus()).append("\n\n")
                .append("Содержимое:\n");
        order.getContent().forEach(orderLine -> {
            Product product = productService.findById(orderLine.getProductId());
            int amount = orderLine.getAmount();
            sb.append(product.getName()).append(" x ").append(amount).append("\n");
        });
        return sb.toString();
    }
}
