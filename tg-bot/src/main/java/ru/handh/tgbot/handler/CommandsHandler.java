package ru.handh.tgbot.handler;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import ru.handh.tgbot.command.*;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Component
@Slf4j
public class CommandsHandler {

    private final Map<String, Command> commands;

    public CommandsHandler(StartCommand startCommand) {
        this.commands = Map.of(
                "/start", startCommand
        );
    }

    public List<BotApiMethod<? extends Serializable>> handleCommands(Update update) {
        String messageText = update.getMessage().getText();
        long chatId = update.getMessage().getChatId();

        Command command = commands.get(messageText);
        if (command != null) {
            return command.apply(update);
        } else {
            return List.of(new SendMessage(String.valueOf(chatId), "Неизвестная команда!"));
        }
    }
}
