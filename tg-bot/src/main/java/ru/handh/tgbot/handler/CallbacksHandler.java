package ru.handh.tgbot.handler;

import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.meta.api.objects.Update;
import ru.handh.tgbot.callback.*;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Component
public class CallbacksHandler {

    private final Map<String, Callback> callbacks;

    public CallbacksHandler(MenuCallback menuCallback,
                            ProfileCallback profileCallback,
                            OrdersCallback ordersCallback,
                            MakeOrderCallback makeOrderCallback,
                            MenuChooseCategoryCallback menuChooseCategoryCallback,
                            StartCallback startCallback,
                            PaymentCallback paymentCallback,
                            ChooseOrderCallback chooseOrderCallback,
                            ChooseProductCallback chooseProductCallback,
                            OrderReadyCallback orderReadyCallback) {
        this.callbacks = Map.of(
                "start", startCallback,
                "menu", menuCallback,
                "profile", profileCallback,
                "orders", ordersCallback,
                "make_order", makeOrderCallback,
                "choose_category", menuChooseCategoryCallback,
                "payment", paymentCallback,
                "choose_order", chooseOrderCallback,
                "choose_product", chooseProductCallback,
                "order_ready", orderReadyCallback
        );
    }

    public List<BotApiMethod<? extends Serializable>> handleCallbacks(Update update) {
        String callbackRaw = update.getCallbackQuery().getData();
        String callbackType = callbackRaw.split(" ")[0];
        long chatId = update.getCallbackQuery().getMessage().getChatId();
        int messageId = update.getCallbackQuery().getMessage().getMessageId();

        Callback callback = callbacks.get(callbackType);
        if (callback != null) {
            return callback.apply(update);
        } else {
            return List.of(
                    EditMessageText.builder()
                            .chatId(chatId)
                            .text("Ошибка!")
                            .messageId(messageId)
                            .build()
            );
        }
    }
}
