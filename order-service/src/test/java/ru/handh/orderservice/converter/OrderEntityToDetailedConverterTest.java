package ru.handh.orderservice.converter;

import org.junit.jupiter.api.Test;
import ru.handh.orderservice.dto.OrderLineDTO;
import ru.handh.orderservice.dto.response.OrderDetailed;
import ru.handh.orderservice.entity.OrderEntity;
import ru.handh.orderservice.entity.OrderLineEntity;
import ru.handh.orderservice.entity.OrderStatus;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class OrderEntityToDetailedConverterTest {

    @Test
    void convert() {
        OrderEntityToDetailedConverter converter = new OrderEntityToDetailedConverter();
        OrderEntity order = OrderEntity.builder()
                .id(1)
                .ownerId(10)
                .status(OrderStatus.READY)
                .build();
        List<OrderLineEntity> orderContent = List.of(
                OrderLineEntity.builder().id(1).productId(5).amount(2).order(order).build(),
                OrderLineEntity.builder().id(2).productId(3).amount(6).order(order).build()
        );
        order.setContent(orderContent);
        List<OrderLineDTO> expectedContent = List.of(
                new OrderLineDTO(5, 2),
                new OrderLineDTO(3, 6)
        );
        OrderDetailed expected = OrderDetailed.builder()
                .id(1)
                .ownerId(10)
                .status(OrderStatus.READY)
                .content(expectedContent)
                .build();

        OrderDetailed actual = converter.convert(order);

        assertEquals(expected, actual);
    }
}