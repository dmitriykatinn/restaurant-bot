package ru.handh.orderservice.service.impl;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.server.ResponseStatusException;
import ru.handh.orderservice.dto.OrderLineDTO;
import ru.handh.orderservice.dto.request.OrderRequest;
import ru.handh.orderservice.dto.request.UpdateOrderStatusRequest;
import ru.handh.orderservice.entity.OrderEntity;
import ru.handh.orderservice.entity.OrderLineEntity;
import ru.handh.orderservice.entity.OrderStatus;
import ru.handh.orderservice.repository.OrderRepository;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class OrderServiceImplTest {
    @InjectMocks
    private OrderServiceImpl orderService;
    @Mock
    private OrderRepository orderRepository;
    @Mock
    private KafkaTemplate<String, Integer> kafkaTemplate;

    @Test
    void createOrder() {
        OrderRequest given = OrderRequest.builder()
                .ownerId(10)
                .status(OrderStatus.READY)
                .content(List.of(new OrderLineDTO(5, 2),
                        new OrderLineDTO(3, 6)))
                .build();
        OrderEntity expected = OrderEntity.builder()
                .ownerId(10)
                .status(OrderStatus.READY)
                .build();
        expected.setContent(List.of(
                OrderLineEntity.builder().productId(5).amount(2).order(expected).build(),
                OrderLineEntity.builder().productId(3).amount(6).order(expected).build()
        ));

        OrderEntity actual = orderService.createOrder(given);

        assertEquals(expected, actual);
    }

    @Test
    void findById() {
        int orderId = 5;
        OrderEntity expected = new OrderEntity(5, 10, OrderStatus.READY, Collections.emptyList());
        Mockito.when(orderRepository.findById(5)).thenReturn(Optional.of(expected));

        OrderEntity actual = orderService.findById(orderId);

        assertEquals(expected, actual);
    }

    @Test
    void findById_shouldThrowsException_whenOrderNotFound() {
        int orderId = 1;
        Mockito.when(orderRepository.findById(orderId)).thenReturn(Optional.empty());

        assertThrows(ResponseStatusException.class, () -> orderService.findById(orderId));
    }

    @Test
    void findByOwnerId() {
        long ownerId = 10;
        List<OrderEntity> expected = List.of(
                new OrderEntity(1, 10, OrderStatus.READY, Collections.emptyList()),
                new OrderEntity(2, 10, OrderStatus.PREPARING, Collections.emptyList())
        );
        Mockito.when(orderRepository.findByOwnerId(10)).thenReturn(expected);

        List<OrderEntity> actual = orderService.findByOwnerId(ownerId);

        assertEquals(expected, actual);
    }

    @Test
    void updateOrder() {
        int orderId = 1;
        UpdateOrderStatusRequest request = new UpdateOrderStatusRequest(OrderStatus.READY);
        OrderEntity order = new OrderEntity(1, 10, OrderStatus.PREPARING, Collections.emptyList());
        Mockito.when(orderRepository.findById(1)).thenReturn(Optional.of(order));
        OrderEntity expected = new OrderEntity(1, 10, OrderStatus.READY, Collections.emptyList());
        Mockito.when(orderRepository.save(expected)).thenReturn(expected);

        OrderEntity actual = orderService.updateOrder(orderId, request);

        assertEquals(expected, actual);
    }

    @Test
    void updateOrder_shouldThrowsException_whenOrderNotFound() {
        int orderId = 1;
        UpdateOrderStatusRequest request = new UpdateOrderStatusRequest(OrderStatus.READY);
        Mockito.when(orderRepository.findById(1)).thenReturn(Optional.empty());

        assertThrows(ResponseStatusException.class, () -> orderService.updateOrder(orderId, request));
    }
}