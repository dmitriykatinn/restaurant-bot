package ru.handh.orderservice.rest;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.ConversionService;
import org.springframework.web.bind.annotation.*;
import ru.handh.orderservice.dto.request.OrderRequest;
import ru.handh.orderservice.dto.request.UpdateOrderStatusRequest;
import ru.handh.orderservice.dto.response.OrderDetailed;
import ru.handh.orderservice.entity.OrderEntity;
import ru.handh.orderservice.service.OrderService;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/orders")
@RequiredArgsConstructor
public class OrderController {

    private final OrderService orderService;
    private final ConversionService conversionService;

    @PostMapping
    public OrderDetailed createOrder(@RequestBody @Valid OrderRequest request) {
        OrderEntity order = orderService.createOrder(request);
        return conversionService.convert(order, OrderDetailed.class);
    }

    @GetMapping("/{orderId}")
    public OrderDetailed getOrder(@PathVariable int orderId) {
        OrderEntity order = orderService.findById(orderId);
        return conversionService.convert(order, OrderDetailed.class);
    }

    @GetMapping
    public List<OrderDetailed> getOrdersListByOwner(@RequestParam long ownerId) {
        return orderService.findByOwnerId(ownerId).stream()
                .map(order -> conversionService.convert(order, OrderDetailed.class))
                .collect(Collectors.toList());
    }

    @PatchMapping("/{orderId}")
    public OrderDetailed updateOrderStatus(@PathVariable int orderId,
                                           @RequestBody @Valid UpdateOrderStatusRequest request) {
        OrderEntity order = orderService.updateOrder(orderId, request);
        return conversionService.convert(order, OrderDetailed.class);
    }
}
