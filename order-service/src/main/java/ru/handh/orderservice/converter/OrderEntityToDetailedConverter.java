package ru.handh.orderservice.converter;

import org.springframework.core.convert.converter.Converter;
import ru.handh.orderservice.dto.OrderLineDTO;
import ru.handh.orderservice.dto.response.OrderDetailed;
import ru.handh.orderservice.entity.OrderEntity;

import java.util.stream.Collectors;

public class OrderEntityToDetailedConverter implements Converter<OrderEntity, OrderDetailed> {
    @Override
    public OrderDetailed convert(OrderEntity source) {
        OrderDetailed orderDetailed = new OrderDetailed();
        orderDetailed.setId(source.getId());
        orderDetailed.setOwnerId(source.getOwnerId());
        orderDetailed.setStatus(source.getStatus());
        orderDetailed.setContent(source.getContent().stream()
                .map(o -> new OrderLineDTO(o.getProductId(), o.getAmount()))
                .collect(Collectors.toList()));
        return orderDetailed;
    }
}
