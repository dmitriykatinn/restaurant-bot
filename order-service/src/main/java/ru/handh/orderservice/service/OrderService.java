package ru.handh.orderservice.service;

import ru.handh.orderservice.dto.request.OrderRequest;
import ru.handh.orderservice.dto.request.UpdateOrderStatusRequest;
import ru.handh.orderservice.entity.OrderEntity;

import java.util.List;

public interface OrderService {
    OrderEntity createOrder(OrderRequest request);

    OrderEntity findById(int orderId);

    List<OrderEntity> findByOwnerId(long ownerId);

    OrderEntity updateOrder(int orderId, UpdateOrderStatusRequest request);
}
