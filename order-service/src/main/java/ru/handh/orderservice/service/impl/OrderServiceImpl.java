package ru.handh.orderservice.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import ru.handh.orderservice.dto.request.OrderRequest;
import ru.handh.orderservice.dto.request.UpdateOrderStatusRequest;
import ru.handh.orderservice.entity.OrderEntity;
import ru.handh.orderservice.entity.OrderLineEntity;
import ru.handh.orderservice.repository.OrderRepository;
import ru.handh.orderservice.service.OrderService;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class OrderServiceImpl implements OrderService {

    private final OrderRepository orderRepository;
    private final KafkaTemplate<String, Integer> kafkaTemplate;

    @Override
    public OrderEntity createOrder(OrderRequest request) {
        OrderEntity order = OrderEntity.builder()
                .ownerId(request.getOwnerId())
                .status(request.getStatus())
                .build();
        order.setContent(request.getContent().stream()
                .map(o -> OrderLineEntity.builder()
                        .productId(o.getProductId())
                        .amount(o.getAmount())
                        .order(order)
                        .build())
                .collect(Collectors.toList()));
        orderRepository.save(order);
        kafkaTemplate.send("ORDER_CREATED", order.getId());
        return order;
    }

    @Override
    public OrderEntity findById(int orderId) {
        return orderRepository.findById(orderId)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST));
    }

    @Override
    public List<OrderEntity> findByOwnerId(long ownerId) {
        return orderRepository.findByOwnerId(ownerId);
    }

    @Override
    public OrderEntity updateOrder(int orderId, UpdateOrderStatusRequest request) {
        OrderEntity order = findById(orderId);
        order.setStatus(request.getStatus());
        kafkaTemplate.send("ORDER_STATUS_UPDATED", order.getId());
        return orderRepository.save(order);
    }
}
