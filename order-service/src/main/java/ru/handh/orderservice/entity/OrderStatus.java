package ru.handh.orderservice.entity;

public enum OrderStatus {
    PREPARING, READY, ISSUED
}
