package ru.handh.orderservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.handh.orderservice.entity.OrderEntity;

import java.util.List;

public interface OrderRepository extends JpaRepository<OrderEntity, Integer> {
    List<OrderEntity> findByOwnerId(long ownerId);
}
