package ru.handh.orderservice.dto.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.handh.orderservice.dto.OrderLineDTO;
import ru.handh.orderservice.entity.OrderStatus;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class OrderDetailed {
    private int id;
    private long ownerId;
    private OrderStatus status;
    private List<OrderLineDTO> content;
}
