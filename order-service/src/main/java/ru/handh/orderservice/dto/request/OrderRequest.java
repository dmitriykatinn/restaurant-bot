package ru.handh.orderservice.dto.request;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.handh.orderservice.dto.OrderLineDTO;
import ru.handh.orderservice.entity.OrderStatus;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class OrderRequest {
    @NotNull
    private long ownerId;

    @NotNull
    private OrderStatus status;

    @NotEmpty
    private List<OrderLineDTO> content;
}
