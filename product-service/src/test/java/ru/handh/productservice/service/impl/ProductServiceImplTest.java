package ru.handh.productservice.service.impl;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.server.ResponseStatusException;
import ru.handh.productservice.dto.request.ProductRequest;
import ru.handh.productservice.entity.CategoryEntity;
import ru.handh.productservice.entity.ProductEntity;
import ru.handh.productservice.repository.CategoryRepository;
import ru.handh.productservice.repository.ProductRepository;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class ProductServiceImplTest {
    @InjectMocks
    private ProductServiceImpl productService;
    @Mock
    private ProductRepository productRepository;
    @Mock
    private CategoryRepository categoryRepository;

    @Test
    void createProduct() {
        ProductRequest given = new ProductRequest("cola", 99, 4);
        CategoryEntity category = CategoryEntity.builder()
                .id(4)
                .name("beverage")
                .build();
        Mockito.when(categoryRepository.findById(4)).thenReturn(Optional.of(category));
        ProductEntity expected = ProductEntity.builder()
                .name("cola")
                .price(99)
                .category(category)
                .build();
        Mockito.when(productRepository.save(expected)).thenReturn(expected);

        ProductEntity actual = productService.createProduct(given);

        assertEquals(expected, actual);
    }

    @Test
    void createProduct_shouldThrowException_whenCategoryNotFound() {
        ProductRequest given = new ProductRequest("cola", 99, 4);
        Mockito.when(categoryRepository.findById(4)).thenReturn(Optional.empty());

        assertThrows(ResponseStatusException.class, () -> productService.createProduct(given));
    }

    @Test
    void editProduct() {
        int productId = 5;
        ProductRequest given = new ProductRequest("cola", 99, 4);
        ProductEntity found = new ProductEntity(5, "fanta", 100, new CategoryEntity());
        Mockito.when(productRepository.findById(5)).thenReturn(Optional.of(found));
        CategoryEntity category = CategoryEntity.builder()
                .id(4)
                .name("beverage")
                .build();
        Mockito.when(categoryRepository.findById(4)).thenReturn(Optional.of(category));
        ProductEntity expected = new ProductEntity(5, "cola", 99, category);
        Mockito.when(productRepository.save(expected)).thenReturn(expected);

        ProductEntity actual = productService.editProduct(productId, given);

        assertEquals(expected, actual);
    }

    @Test
    void editProduct_shouldThrowException_whenProductNotFound() {
        int productId = 5;
        ProductRequest given = new ProductRequest("cola", 99, 4);
        Mockito.when(productRepository.findById(5)).thenReturn(Optional.empty());

        assertThrows(ResponseStatusException.class, () -> productService.editProduct(productId, given));
    }

    @Test
    void editProduct_shouldThrowException_whenCategoryNotFound() {
        int productId = 5;
        ProductRequest given = new ProductRequest("cola", 99, 4);
        ProductEntity found = new ProductEntity(5, "fanta", 100, new CategoryEntity());
        Mockito.when(productRepository.findById(5)).thenReturn(Optional.of(found));
        Mockito.when(categoryRepository.findById(4)).thenReturn(Optional.empty());

        assertThrows(ResponseStatusException.class, () -> productService.editProduct(productId, given));
    }

    @Test
    void findById() {
        int productId = 5;
        ProductEntity expected = new ProductEntity(5, "fanta", 100, new CategoryEntity());
        Mockito.when(productRepository.findById(5)).thenReturn(Optional.of(expected));

        ProductEntity actual = productService.findById(productId);

        assertEquals(expected, actual);
    }

    @Test
    void findById_shouldThrowException_whenProductNotFound() {
        int productId = 1;
        Mockito.when(productRepository.findById(productId)).thenReturn(Optional.empty());

        assertThrows(ResponseStatusException.class, () -> productService.findById(productId));
    }

    @Test
    void findByCategoryId() {
        int categoryId = 5;
        CategoryEntity category = CategoryEntity.builder()
                .id(5)
                .name("beverage")
                .build();
        List<ProductEntity> expected = List.of(
                new ProductEntity(5, "fanta", 100, category),
                new ProductEntity(3, "cola", 10, category)
        );
        Mockito.when(productRepository.findByCategoryId(5)).thenReturn(expected);

        List<ProductEntity> actual = productService.findByCategoryId(categoryId);

        assertEquals(expected, actual);
    }

    @Test
    void deleteById() {
        int productId = 1;
        productService.deleteById(productId);

        Mockito.verify(productRepository).deleteById(1);
    }
}