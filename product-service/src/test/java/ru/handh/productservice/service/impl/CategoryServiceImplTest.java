package ru.handh.productservice.service.impl;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.server.ResponseStatusException;
import ru.handh.productservice.dto.request.CategoryRequest;
import ru.handh.productservice.entity.CategoryEntity;
import ru.handh.productservice.entity.ProductEntity;
import ru.handh.productservice.repository.CategoryRepository;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class CategoryServiceImplTest {
    @InjectMocks
    private CategoryServiceImpl categoryService;
    @Mock
    private CategoryRepository categoryRepository;

    @Test
    void createCategory() {
        CategoryRequest given = new CategoryRequest("burger");
        CategoryEntity expected = new CategoryEntity();
        expected.setName("burger");
        Mockito.when(categoryRepository.save(expected)).thenReturn(expected);

        CategoryEntity actual = categoryService.createCategory(given);

        assertEquals(expected, actual);
    }

    @Test
    void editCategory() {
        int categoryId = 5;
        CategoryRequest request = new CategoryRequest("burger");
        CategoryEntity found = new CategoryEntity(5, "beverage", Collections.emptyList());
        CategoryEntity expected = new CategoryEntity(5, "burger", Collections.emptyList());
        Mockito.when(categoryRepository.findById(5)).thenReturn(Optional.of(found));
        Mockito.when(categoryRepository.save(expected)).thenReturn(expected);

        CategoryEntity actual = categoryService.editCategory(categoryId, request);

        assertEquals(expected, actual);
    }

    @Test
    void editCategory_shouldThrowsException_whenCategoryNotFound() {
        int categoryId = 5;
        CategoryRequest request = new CategoryRequest("burger");
        Mockito.when(categoryRepository.findById(5)).thenReturn(Optional.empty());

        assertThrows(ResponseStatusException.class, () -> categoryService.editCategory(categoryId, request));
    }

    @Test
    void findById() {
        int categoryId = 5;
        CategoryEntity expected = CategoryEntity.builder()
                .id(5)
                .name("burger")
                .build();
        Mockito.when(categoryRepository.findById(5)).thenReturn(Optional.of(expected));

        CategoryEntity actual = categoryService.findById(categoryId);

        assertEquals(expected, actual);
    }

    @Test
    void findById_shouldThrowException_whenProductNotFound() {
        int categoryId = 1;
        Mockito.when(categoryRepository.findById(categoryId)).thenReturn(Optional.empty());

        assertThrows(ResponseStatusException.class, () -> categoryService.findById(categoryId));
    }

    @Test
    void findAll() {
        List<CategoryEntity> expected = List.of(
                new CategoryEntity(1, "burger", Collections.emptyList()),
                new CategoryEntity(2, "beverage", Collections.emptyList())
        );
        Mockito.when(categoryRepository.findAll()).thenReturn(expected);

        List<CategoryEntity> actual = categoryService.findAll();

        assertEquals(expected, actual);
    }

    @Test
    void deleteById() {
        int categoryId = 1;
        categoryService.deleteById(categoryId);

        Mockito.verify(categoryRepository).deleteById(1);
    }
}