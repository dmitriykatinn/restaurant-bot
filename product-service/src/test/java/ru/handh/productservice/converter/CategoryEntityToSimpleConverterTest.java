package ru.handh.productservice.converter;

import org.junit.jupiter.api.Test;
import ru.handh.productservice.dto.response.CategoryDetailed;
import ru.handh.productservice.dto.response.CategorySimple;
import ru.handh.productservice.entity.CategoryEntity;
import ru.handh.productservice.entity.ProductEntity;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class CategoryEntityToSimpleConverterTest {

    @Test
    void convert() {
        CategoryEntityToSimpleConverter converter = new CategoryEntityToSimpleConverter();
        CategoryEntity category = CategoryEntity.builder()
                .id(1)
                .name("burger")
                .products(List.of(new ProductEntity()))
                .build();
        CategorySimple expected = new CategorySimple(1, "burger");

        CategorySimple actual = converter.convert(category);

        assertEquals(expected, actual);
    }
}