package ru.handh.productservice.converter;

import org.junit.jupiter.api.Test;
import ru.handh.productservice.dto.response.CategoryDetailed;
import ru.handh.productservice.entity.CategoryEntity;
import ru.handh.productservice.entity.ProductEntity;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class CategoryEntityToDetailedConverterTest {

    @Test
    void convert() {
        CategoryEntityToDetailedConverter converter = new CategoryEntityToDetailedConverter();
        CategoryEntity category = CategoryEntity.builder()
                .id(1)
                .name("burger")
                .products(List.of(new ProductEntity()))
                .build();
        CategoryDetailed expected = new CategoryDetailed(1, "burger");

        CategoryDetailed actual = converter.convert(category);

        assertEquals(expected, actual);
    }
}