package ru.handh.productservice.converter;

import org.junit.jupiter.api.Test;
import ru.handh.productservice.dto.response.ProductDetailed;
import ru.handh.productservice.dto.response.ProductSimple;
import ru.handh.productservice.entity.CategoryEntity;
import ru.handh.productservice.entity.ProductEntity;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;

class ProductEntityToSimpleConverterTest {

    @Test
    void convert() {
        ProductEntityToSimpleConverter converter = new ProductEntityToSimpleConverter();
        ProductEntity product = ProductEntity.builder()
                .id(6)
                .name("big chicken")
                .price(199)
                .category(new CategoryEntity(5, "burger", Collections.emptyList()))
                .build();
        ProductSimple expected = new ProductSimple(6, "big chicken", 199);

        ProductSimple actual = converter.convert(product);

        assertEquals(expected, actual);
    }
}