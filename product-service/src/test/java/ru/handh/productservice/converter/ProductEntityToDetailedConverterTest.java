package ru.handh.productservice.converter;

import org.junit.jupiter.api.Test;
import ru.handh.productservice.dto.response.CategoryDetailed;
import ru.handh.productservice.dto.response.ProductDetailed;
import ru.handh.productservice.entity.CategoryEntity;
import ru.handh.productservice.entity.ProductEntity;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ProductEntityToDetailedConverterTest {

    @Test
    void convert() {
        ProductEntityToDetailedConverter converter = new ProductEntityToDetailedConverter();
        ProductEntity product = ProductEntity.builder()
                .id(6)
                .name("big chicken")
                .price(199)
                .category(new CategoryEntity(5, "burger", Collections.emptyList()))
                .build();
        ProductDetailed expected = new ProductDetailed(6, "big chicken", 199, 5);

        ProductDetailed actual = converter.convert(product);

        assertEquals(expected, actual);
    }
}