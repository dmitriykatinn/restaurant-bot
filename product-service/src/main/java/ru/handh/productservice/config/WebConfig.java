package ru.handh.productservice.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import ru.handh.productservice.converter.CategoryEntityToDetailedConverter;
import ru.handh.productservice.converter.CategoryEntityToSimpleConverter;
import ru.handh.productservice.converter.ProductEntityToDetailedConverter;
import ru.handh.productservice.converter.ProductEntityToSimpleConverter;

@Configuration
public class WebConfig implements WebMvcConfigurer {
    @Override
    public void addFormatters(FormatterRegistry registry) {
        registry.addConverter(new ProductEntityToDetailedConverter());
        registry.addConverter(new ProductEntityToSimpleConverter());
        registry.addConverter(new CategoryEntityToDetailedConverter());
        registry.addConverter(new CategoryEntityToSimpleConverter());
    }
}
