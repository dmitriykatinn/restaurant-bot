package ru.handh.productservice.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import ru.handh.productservice.dto.request.CategoryRequest;
import ru.handh.productservice.entity.CategoryEntity;
import ru.handh.productservice.repository.CategoryRepository;
import ru.handh.productservice.service.CategoryService;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CategoryServiceImpl implements CategoryService {

    private final CategoryRepository categoryRepository;

    @Override
    public CategoryEntity createCategory(CategoryRequest categoryRequest) {
        CategoryEntity category = CategoryEntity.builder()
                .name(categoryRequest.getName())
                .build();
        return categoryRepository.save(category);
    }

    @Override
    public CategoryEntity editCategory(int categoryId, CategoryRequest categoryRequest) {
        CategoryEntity category = categoryRepository.findById(categoryId)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST));
        category.setName(categoryRequest.getName());
        return categoryRepository.save(category);
    }

    @Override
    public CategoryEntity findById(int categoryId) {
        return categoryRepository.findById(categoryId)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST));
    }

    @Override
    public List<CategoryEntity> findAll() {
        return categoryRepository.findAll();
    }

    @Override
    public void deleteById(int categoryId) {
        categoryRepository.deleteById(categoryId);
    }
}
