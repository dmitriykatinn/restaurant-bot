package ru.handh.productservice.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import ru.handh.productservice.dto.request.ProductRequest;
import ru.handh.productservice.entity.CategoryEntity;
import ru.handh.productservice.entity.ProductEntity;
import ru.handh.productservice.repository.CategoryRepository;
import ru.handh.productservice.repository.ProductRepository;
import ru.handh.productservice.service.ProductService;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;
    private final CategoryRepository categoryRepository;

    @Override
    public ProductEntity createProduct(ProductRequest productRequest) {
        CategoryEntity category = categoryRepository.findById(productRequest.getCategoryId())
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST));
        ProductEntity product = ProductEntity.builder()
                .name(productRequest.getName())
                .price(productRequest.getPrice())
                .category(category)
                .build();
        return productRepository.save(product);
    }

    @Override
    public ProductEntity editProduct(int productId, ProductRequest productRequest) {
        ProductEntity product = productRepository.findById(productId)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST));
        CategoryEntity category = categoryRepository.findById(productRequest.getCategoryId())
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST));
        product.setName(productRequest.getName());
        product.setPrice(productRequest.getPrice());
        product.setCategory(category);
        return productRepository.save(product);
    }

    @Override
    public ProductEntity findById(int productId) {
        return productRepository.findById(productId)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST));
    }

    @Override
    public List<ProductEntity> findByCategoryId(int categoryId) {
        return productRepository.findByCategoryId(categoryId);
    }

    @Override
    public void deleteById(int productId) {
        productRepository.deleteById(productId);
    }
}
