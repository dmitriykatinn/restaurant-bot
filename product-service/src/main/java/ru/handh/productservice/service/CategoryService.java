package ru.handh.productservice.service;

import ru.handh.productservice.dto.request.CategoryRequest;
import ru.handh.productservice.entity.CategoryEntity;

import java.util.Arrays;
import java.util.List;

public interface CategoryService {
    CategoryEntity createCategory(CategoryRequest categoryRequest);

    CategoryEntity editCategory(int categoryId, CategoryRequest categoryRequest);

    CategoryEntity findById(int categoryId);

    List<CategoryEntity> findAll();

    void deleteById(int categoryId);
}
