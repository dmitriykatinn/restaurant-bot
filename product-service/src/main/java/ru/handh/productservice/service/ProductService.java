package ru.handh.productservice.service;

import ru.handh.productservice.dto.request.ProductRequest;
import ru.handh.productservice.entity.ProductEntity;

import java.util.Arrays;
import java.util.List;

public interface ProductService {
    ProductEntity createProduct(ProductRequest productRequest);

    ProductEntity editProduct(int productId, ProductRequest productRequest);

    ProductEntity findById(int productId);

    List<ProductEntity> findByCategoryId(int categoryId);

    void deleteById(int productId);
}
