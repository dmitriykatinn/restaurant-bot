package ru.handh.productservice.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductDetailed {
    private int id;
    private String name;
    private double price;
    private int categoryId;
}
