package ru.handh.productservice.rest;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.ConversionService;
import org.springframework.web.bind.annotation.*;
import ru.handh.productservice.dto.request.CategoryRequest;
import ru.handh.productservice.dto.response.CategoryDetailed;
import ru.handh.productservice.dto.response.CategorySimple;
import ru.handh.productservice.entity.CategoryEntity;
import ru.handh.productservice.entity.ProductEntity;
import ru.handh.productservice.service.CategoryService;
import ru.handh.productservice.service.ProductService;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/products/categories")
@RequiredArgsConstructor
public class CategoryController {

    private final CategoryService categoryService;
    private final ConversionService conversionService;

    @PostMapping
    public CategoryDetailed createCategory(@RequestBody @Valid CategoryRequest categoryRequest) {
        CategoryEntity categoryCreated = categoryService.createCategory(categoryRequest);
        return conversionService.convert(categoryCreated, CategoryDetailed.class);
    }

    @PutMapping("/{categoryId}")
    public CategoryDetailed editCategory(@PathVariable int categoryId,
                                       @RequestBody @Valid CategoryRequest categoryRequest) {
        CategoryEntity categoryEdited = categoryService.editCategory(categoryId, categoryRequest);
        return conversionService.convert(categoryEdited, CategoryDetailed.class);
    }

    @GetMapping("/{categoryId}")
    public CategoryDetailed getCategory(@PathVariable int categoryId) {
        CategoryEntity category = categoryService.findById(categoryId);
        return conversionService.convert(category, CategoryDetailed.class);
    }

    @GetMapping
    public List<CategorySimple> getCategoriesList() {
        return categoryService.findAll().stream()
                .map(category -> conversionService.convert(category, CategorySimple.class))
                .collect(Collectors.toList());
    }

    @DeleteMapping("/{categoryId}")
    public void deleteCategory(@PathVariable int categoryId) {
        categoryService.deleteById(categoryId);
    }
}
