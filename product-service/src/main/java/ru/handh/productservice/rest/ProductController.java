package ru.handh.productservice.rest;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.ConversionService;
import org.springframework.web.bind.annotation.*;
import ru.handh.productservice.dto.request.ProductRequest;
import ru.handh.productservice.dto.response.ProductDetailed;
import ru.handh.productservice.dto.response.ProductSimple;
import ru.handh.productservice.entity.ProductEntity;
import ru.handh.productservice.service.ProductService;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/products")
@RequiredArgsConstructor
public class ProductController {

    private final ProductService productService;
    private final ConversionService conversionService;

    @PostMapping
    public ProductDetailed createProduct(@RequestBody @Valid ProductRequest productRequest) {
        ProductEntity productCreated = productService.createProduct(productRequest);
        return conversionService.convert(productCreated, ProductDetailed.class);
    }

    @PutMapping("/{productId}")
    public ProductDetailed editProduct(@PathVariable int productId,
                                       @RequestBody @Valid ProductRequest productRequest) {
        ProductEntity productEdited = productService.editProduct(productId, productRequest);
        return conversionService.convert(productEdited, ProductDetailed.class);
    }

    @GetMapping("/{productId}")
    public ProductDetailed getProduct(@PathVariable int productId) {
        ProductEntity product = productService.findById(productId);
        return conversionService.convert(product, ProductDetailed.class);
    }

    @GetMapping
    public List<ProductSimple> getProductsListByCategory(@RequestParam int categoryId) {
        return productService.findByCategoryId(categoryId).stream()
                .map(product -> conversionService.convert(product, ProductSimple.class))
                .collect(Collectors.toList());
    }

    @DeleteMapping("/{productId}")
    public void deleteProduct(@PathVariable int productId) {
        productService.deleteById(productId);
    }
}
