package ru.handh.productservice.converter;

import org.springframework.core.convert.converter.Converter;
import ru.handh.productservice.dto.response.ProductDetailed;
import ru.handh.productservice.entity.ProductEntity;

public class ProductEntityToDetailedConverter implements Converter<ProductEntity, ProductDetailed> {
    @Override
    public ProductDetailed convert(ProductEntity source) {
        return new ProductDetailed(source.getId(), source.getName(), source.getPrice(), source.getCategory().getId());
    }
}
