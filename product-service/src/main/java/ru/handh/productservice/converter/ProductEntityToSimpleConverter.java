package ru.handh.productservice.converter;

import org.springframework.core.convert.converter.Converter;
import ru.handh.productservice.dto.response.ProductSimple;
import ru.handh.productservice.entity.ProductEntity;

public class ProductEntityToSimpleConverter implements Converter<ProductEntity, ProductSimple> {
    @Override
    public ProductSimple convert(ProductEntity source) {
        return new ProductSimple(source.getId(), source.getName(), source.getPrice());
    }
}
