package ru.handh.productservice.converter;

import org.springframework.core.convert.converter.Converter;
import ru.handh.productservice.dto.response.CategoryDetailed;
import ru.handh.productservice.entity.CategoryEntity;

public class CategoryEntityToDetailedConverter implements Converter<CategoryEntity, CategoryDetailed> {
    @Override
    public CategoryDetailed convert(CategoryEntity source) {
        return new CategoryDetailed(source.getId(), source.getName());
    }
}
