package ru.handh.productservice.converter;

import org.springframework.core.convert.converter.Converter;
import ru.handh.productservice.dto.response.CategorySimple;
import ru.handh.productservice.entity.CategoryEntity;

public class CategoryEntityToSimpleConverter implements Converter<CategoryEntity, CategorySimple> {
    @Override
    public CategorySimple convert(CategoryEntity source) {
        return new CategorySimple(source.getId(), source.getName());
    }
}
