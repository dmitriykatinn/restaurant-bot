package ru.handh.userservice.service.impl;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.server.ResponseStatusException;
import ru.handh.userservice.dto.request.CreateUserRequest;
import ru.handh.userservice.dto.request.PatchUserRequest;
import ru.handh.userservice.entity.Role;
import ru.handh.userservice.entity.UserEntity;
import ru.handh.userservice.repository.UserRepository;
import ru.handh.userservice.util.TimeUtil;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class UserServiceImplTest {

    @InjectMocks
    private UserServiceImpl userService;
    @Mock
    private UserRepository userRepository;
    @Mock
    private TimeUtil timeUtil;

    @Test
    void createUser() {
        CreateUserRequest given = CreateUserRequest.builder()
                .id(1)
                .name("user")
                .role(Role.CLIENT)
                .bonusPoints(100)
                .chatId(20)
                .build();
        Instant time = Instant.ofEpochSecond(123);
        UserEntity expected = UserEntity.builder()
                .id(1)
                .name("user")
                .role(Role.CLIENT)
                .bonusPoints(100)
                .chatId(20)
                .lastAction(time)
                .build();
        Mockito.when(timeUtil.now()).thenReturn(time);
        Mockito.when(userRepository.save(expected)).thenReturn(expected);

        UserEntity actual = userService.createUser(given);

        assertEquals(expected, actual);
    }

    @Test
    void findById() {
        long userId = 1;
        UserEntity expected = UserEntity.builder()
                .id(1)
                .name("user")
                .role(Role.CLIENT)
                .bonusPoints(100)
                .chatId(20)
                .lastAction(Instant.ofEpochSecond(123))
                .build();
        Mockito.when(userRepository.findById(userId)).thenReturn(Optional.of(expected));

        UserEntity actual = userService.findById(userId);

        assertEquals(expected, actual);
    }

    @Test
    void findById_shouldThrowsException_whenUserNotFound() {
        long userId = 1;
        Mockito.when(userRepository.findById(userId)).thenReturn(Optional.empty());

        assertThrows(ResponseStatusException.class, () -> userService.findById(userId));
    }

    @Test
    void findByRole() {
        Role given = Role.CLIENT;
        List<UserEntity> expected = List.of(
                UserEntity.builder().id(1).role(Role.CLIENT).build(),
                UserEntity.builder().id(2).role(Role.CLIENT).build()
        );
        Mockito.when(userRepository.findByRole(given)).thenReturn(expected);

        List<UserEntity> actual = userService.findByRole(given);

        assertEquals(expected, actual);
    }

    @Test
    void findAll() {
        List<UserEntity> expected = List.of(
                UserEntity.builder().id(1).build(),
                UserEntity.builder().id(2).build()
        );
        Mockito.when(userRepository.findAll()).thenReturn(expected);

        List<UserEntity> actual = userService.findAll();

        assertEquals(expected, actual);
    }

    @Test
    void updateUser() {
        long userId = 1;
        PatchUserRequest request = new PatchUserRequest(Role.EMPLOYEE, 100, Instant.ofEpochSecond(123));
        UserEntity userFound = UserEntity.builder()
                .id(1)
                .name("user")
                .role(Role.CLIENT)
                .bonusPoints(50)
                .chatId(20)
                .lastAction(Instant.ofEpochSecond(100))
                .build();
        Mockito.when(userRepository.findById(userId)).thenReturn(Optional.of(userFound));
        UserEntity expected = UserEntity.builder()
                .id(1)
                .name("user")
                .role(Role.EMPLOYEE)
                .bonusPoints(100)
                .chatId(20)
                .lastAction(Instant.ofEpochSecond(123))
                .build();
        Mockito.when(userRepository.save(expected)).thenReturn(expected);

        UserEntity actual = userService.updateUser(userId, request);

        assertEquals(expected, actual);
    }

    @Test
    void updateUser_shouldThrowsException_whenUserNotExists() {
        long userId = 1;
        PatchUserRequest request = new PatchUserRequest();
        request.setRole(Role.EMPLOYEE);
        Mockito.when(userRepository.findById(userId)).thenReturn(Optional.empty());

        assertThrows(ResponseStatusException.class, () -> userService.updateUser(userId, request));
    }
}