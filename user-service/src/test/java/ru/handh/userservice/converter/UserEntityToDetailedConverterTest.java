package ru.handh.userservice.converter;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import ru.handh.userservice.dto.response.UserDetailed;
import ru.handh.userservice.entity.Role;
import ru.handh.userservice.entity.UserEntity;

import java.time.Instant;

import static org.junit.jupiter.api.Assertions.*;

class UserEntityToDetailedConverterTest {

    @Test
    void convert() {
        UserEntityToDetailedConverter converter = new UserEntityToDetailedConverter();
        UserEntity given = UserEntity.builder()
                .id(10)
                .name("user")
                .role(Role.CLIENT)
                .bonusPoints(100)
                .chatId(20)
                .lastAction(Instant.ofEpochSecond(100))
                .build();
        UserDetailed expected = UserDetailed.builder()
                .id(10)
                .name("user")
                .role(Role.CLIENT)
                .bonusPoints(100)
                .chatId(20)
                .lastAction(Instant.ofEpochSecond(100))
                .build();

        UserDetailed actual = converter.convert(given);

        assertEquals(expected, actual);
    }
}