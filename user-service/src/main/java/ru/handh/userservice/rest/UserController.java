package ru.handh.userservice.rest;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.ConversionService;
import org.springframework.web.bind.annotation.*;
import ru.handh.userservice.dto.request.CreateUserRequest;
import ru.handh.userservice.dto.request.PatchUserRequest;
import ru.handh.userservice.dto.response.UserDetailed;
import ru.handh.userservice.entity.Role;
import ru.handh.userservice.entity.UserEntity;
import ru.handh.userservice.service.UserService;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/users")
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;
    private final ConversionService conversionService;

    @PostMapping
    public UserDetailed createUser(@RequestBody @Valid CreateUserRequest request) {
        UserEntity user = userService.createUser(request);
        return conversionService.convert(user, UserDetailed.class);
    }

    @GetMapping("/{userId}")
    public UserDetailed getUser(@PathVariable long userId) {
        UserEntity user = userService.findById(userId);
        return conversionService.convert(user, UserDetailed.class);
    }

    @GetMapping
    public List<UserDetailed> getUsersList(@RequestParam Optional<Role> role) {
        List<UserEntity> users = role.isEmpty() ? userService.findAll() : userService.findByRole(role.get());
        return users.stream()
                .map(user -> conversionService.convert(user, UserDetailed.class))
                .collect(Collectors.toList());
    }

    @PatchMapping("/{userId}")
    public UserDetailed updateUser(@PathVariable long userId,
                                   @RequestBody @Valid PatchUserRequest request) {
        UserEntity user = userService.updateUser(userId, request);
        return conversionService.convert(user, UserDetailed.class);
    }

}
