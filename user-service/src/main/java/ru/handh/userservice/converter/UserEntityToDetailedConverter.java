package ru.handh.userservice.converter;

import org.springframework.core.convert.converter.Converter;
import ru.handh.userservice.dto.response.UserDetailed;
import ru.handh.userservice.entity.UserEntity;

public class UserEntityToDetailedConverter implements Converter<UserEntity, UserDetailed> {
    @Override
    public UserDetailed convert(UserEntity source) {
        return new UserDetailed(source.getId(), source.getName(),
                source.getRole(), source.getBonusPoints(), source.getChatId(), source.getLastAction());
    }
}
