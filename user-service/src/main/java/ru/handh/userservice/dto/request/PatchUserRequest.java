package ru.handh.userservice.dto.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.handh.userservice.entity.Role;

import java.time.Instant;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PatchUserRequest {
    private Role role;
    private Integer bonusPoints;
    private Instant lastAction;
}
