package ru.handh.userservice.dto.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.handh.userservice.entity.Role;

import java.time.Instant;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserDetailed {
    private long id;
    private String name;
    private Role role;
    private int bonusPoints;
    private long chatId;
    private Instant lastAction;
}
