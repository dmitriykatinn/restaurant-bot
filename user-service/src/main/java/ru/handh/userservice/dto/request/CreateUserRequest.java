package ru.handh.userservice.dto.request;

import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.handh.userservice.entity.Role;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CreateUserRequest {
    @NotNull
    private long id;
    @NotNull
    private String name;
    @NotNull
    private Role role;
    @NotNull
    private int bonusPoints;
    @NotNull
    private long chatId;
}
