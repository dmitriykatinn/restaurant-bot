package ru.handh.userservice.util;

import org.springframework.stereotype.Component;

import java.time.Instant;

@Component
public class TimeUtil {

    public Instant now() {
        return Instant.now();
    }
}
