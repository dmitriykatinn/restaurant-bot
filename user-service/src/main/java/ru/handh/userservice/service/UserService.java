package ru.handh.userservice.service;

import ru.handh.userservice.dto.request.CreateUserRequest;
import ru.handh.userservice.dto.request.PatchUserRequest;
import ru.handh.userservice.dto.response.UserDetailed;
import ru.handh.userservice.entity.Role;
import ru.handh.userservice.entity.UserEntity;

import java.util.Arrays;
import java.util.List;

public interface UserService {
    UserEntity createUser(CreateUserRequest request);
    UserEntity findById(long userId);
    UserEntity updateUser(long userId, PatchUserRequest request);
    List<UserEntity> findByRole(Role role);
    List<UserEntity> findAll();
}
