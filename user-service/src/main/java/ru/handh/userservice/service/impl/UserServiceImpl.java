package ru.handh.userservice.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import ru.handh.userservice.dto.request.CreateUserRequest;
import ru.handh.userservice.dto.request.PatchUserRequest;
import ru.handh.userservice.entity.Role;
import ru.handh.userservice.entity.UserEntity;
import ru.handh.userservice.repository.UserRepository;
import ru.handh.userservice.service.UserService;
import ru.handh.userservice.util.TimeUtil;

import java.time.Instant;
import java.util.List;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final TimeUtil timeUtil;

    @Override
    public UserEntity createUser(CreateUserRequest request) {
        UserEntity user = UserEntity.builder()
                .id(request.getId())
                .name(request.getName())
                .role(request.getRole())
                .bonusPoints(request.getBonusPoints())
                .chatId(request.getChatId())
                .lastAction(timeUtil.now())
                .build();
        return userRepository.save(user);
    }

    @Override
    public UserEntity findById(long userId) {
        return userRepository.findById(userId)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST));
    }

    @Override
    public List<UserEntity> findByRole(Role role) {
        return userRepository.findByRole(role);
    }

    @Override
    public List<UserEntity> findAll() {
        return userRepository.findAll();
    }

    @Override
    public UserEntity updateUser(long userId, PatchUserRequest request) {
        UserEntity user = findById(userId);
        if(request.getRole() != null) {
            user.setRole(request.getRole());
        }
        if(request.getBonusPoints() != null) {
            user.setBonusPoints(request.getBonusPoints());
        }
        if(request.getLastAction() != null) {
            user.setLastAction(request.getLastAction());
        }
        return userRepository.save(user);
    }
}
