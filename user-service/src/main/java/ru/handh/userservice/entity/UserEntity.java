package ru.handh.userservice.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;

@Entity
@Table(name = "users")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserEntity {

    @Id
    private long id;

    @NotNull
    @Column
    private String name;

    @NotNull
    @Column
    @Enumerated(EnumType.STRING)
    private Role role;

    @NotNull
    @Column
    @Min(0)
    private int bonusPoints;

    @NotNull
    @Column
    private long chatId;

    @NotNull
    @Column
    private Instant lastAction;
}
