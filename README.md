# restaurant-bot

## Description
Телеграм-бот для ресторана быстрого питания.

## Visuals
Использование бота с точки зрения клиента:

[![](https://i.ytimg.com/vi_webp/BU0dcKG5G-A/oar2.webp)](https://www.youtube.com/shorts/BU0dcKG5G-A)

Использование бота с точки зрения работника:

[![](https://i.ytimg.com/vi/hmmU6S-yqvM/oar2.jpg?sqp=-oaymwEdCJ4EENAFSFWQAgHyq4qpAwwIARUAAIhCcAHAAQY=&rs=AOn4CLAfMwlsmdZXymLINc58GeyXSUgQBQ)](https://www.youtube.com/shorts/hmmU6S-yqvM)
